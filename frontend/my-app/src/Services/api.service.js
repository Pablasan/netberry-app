import axios from 'axios';


class ApiService {

  newTask({
   task_name, categories
  }) {
    return axios.post('http://127.0.0.01:8000/api/task', {
       task_name, categories
    },{
        headers:{},
    }
    
    );
  
  }

  getTasks() {
    return axios.get('http://127.0.0.01:8000/api/task', {
     
    },{
        headers:{},
    }
    
    );
    
  
  }

  deleteTask({task_uid}) {
    return axios.delete(`http://127.0.0.01:8000/api/task/${task_uid}`, {
    
    },{
        headers:{},
    }
    
    );
    
  
  }


 
}

export default new ApiService();