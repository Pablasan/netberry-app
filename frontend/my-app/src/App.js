import React, { useState, useEffect } from "react";
import ApiService from "./Services/api.service";
import "./App.scss";

function App() {
  const [task_name, setTaskName] = useState("");
  const [task_uid, setTaskUid] = useState("");
  const [categories, setCategories] = useState([]);
  const [getTasks, setGetTasks] = useState();

  useEffect(() => {
    ApiService.getTasks()
      .then((response) => {
        setGetTasks(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);

  function inputOnchange(ev) {
    setTaskName(ev.target.value);
  }

  function checkOnchange(ev) {
    setCategories((category) => [
      ...category,
      { category_uid: ev.target.value },
    ]);
  }

  function sendTask() {
    return ApiService.newTask({ task_name, categories })
      .then((response) => {
        console.log(response);
        setGetTasks(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  function remove(buttonUid) {
    setTaskUid(buttonUid);
    return ApiService.deleteTask({ task_uid })
      .then((response) => {
        console.log(response);
        setGetTasks(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <div className="container">
      <header className="App-header">
        <title1>GESTOR DE TAREAS</title1>
      </header>
      <div className="crud">
        <div className="input">
          <input
            type="text"
            class="form-control"
            name="task_name"
            onChange={inputOnchange}
            placeholder="Escribe tarea...."
          />
        </div>
        <div className="Radio-butons">
          <label>
            <input
              type="radio"
              value="c0a51a44-1ede-11ec-9621-0242ac130002"
              onChange={checkOnchange}
            />
            PHP
          </label>
          <label>
            <input
              type="radio"
              name="category"
              value="c56a8b0e-1ede-11ec-9621-0242ac130002"
              onChange={checkOnchange}
            />
            Javascript
          </label>
          <label>
            <input
              type="radio"
              value="cae8a02a-1ede-11ec-9621-0242ac130002"
              onChange={checkOnchange}
            />
            CSS
          </label>
        </div>

        <div className="boton">
          <button type="button" onClick={sendTask}>
            {" "}
            ENVIAR{" "}
          </button>
        </div>
      </div>
      <div className="container-tasks">

        <div className="column"><h2>NOMBRE TAREA</h2>   {getTasks?.map((task) => {

          return (<div className="cell">{task?.task_name} </div>)
        })}</div>

        <div className="column"><h2>CATEGORIA</h2> {getTasks?.map((category) => {
          const categories = category?.categories
          return (<div className="cell-category"> {categories?.map((category_name) => {
            return (<div className="category-name">{category_name?.category_name}</div>)
          })} </div>)
        })}</div>
        <div className="column"><h2>ACCIÓN</h2>
          {getTasks?.map((button) => {
            const buttonUid = button.task_uid
            return (<div className="cell"> <button onClick={() => remove(buttonUid)} type="column">ELIMINAR</button></div>)
          })}
        </div>

      </div>
    </div>
  );
}

export default App;
