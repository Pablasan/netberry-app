<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'category_uid' => 'c0a51a44-1ede-11ec-9621-0242ac130002',
            'category_name' => 'PHP'
        ]);

        DB::table('categories')->insert([
            'category_uid' => 'c56a8b0e-1ede-11ec-9621-0242ac130002',
            'category_name' => 'JavaScript'
        ]);

        DB::table('categories')->insert([
            'category_uid' => 'cae8a02a-1ede-11ec-9621-0242ac130002',
            'category_name' => 'CSS'
        ]);
    }
}
