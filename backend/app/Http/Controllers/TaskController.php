<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Services\TaskService;
use App\Services\CategoryService;
use App\Services\CategoryTaskService;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class TaskController extends Controller
{

    protected $taskService;
    protected $categoryService;
    protected $categoryTaskService;


    /**
     * Create a new controller instance.
     *
     * @param  \App\Services\TaskService  $taskService
     * @return void
     */

    public function __construct(TaskService $taskService, CategoryService $categoryService, CategoryTaskService $categoryTaskService)
    {
        $this->taskService = $taskService;
        $this->categoryService = $categoryService;
        $this->categoryTaskService = $categoryTaskService;
    }

    public function createTask(Request $request)
    {
        try {
            $request->validate([
                'task_name' => 'required|string',
                // 'category_uuid' => 'required'
            ]);
            Log::info('Task Validation passed succesfully');
        } catch (ValidationException $e) {
            Log::error('Task Validation failed -> ' . $e->getMessage());
            return response('Task name is a required field -> ' . $e->getMessage(), 422);
        }

        try {
            DB::beginTransaction();
            $task = $this->taskService->createTask($request->task_name);

            foreach ($request->categories as $category_uid) {

                if (!$this->categoryService->getCategory($category_uid['category_uid'])) {

                    DB::rollBack();
                    Log::error('The category must exist in our db');
                    return response('The category must exist in our db', 422);
                }

                $this->categoryTaskService->createCategoryTask($category_uid['category_uid'], $task->task_uid);
            }

            $tasks = $this->taskService->getAllTasks();
            DB::commit();
            Log::info('Task created succesfully -> '.$tasks);
            return response($tasks, 200);
        }catch(ModelNotFoundException $e){
            DB::rollBack();
            Log::error('Error creating the task -> ' . $e->getMessage());
            return response('Error creating the task -> ' . $e->getMessage(), 400);
        }catch (\Exception $e) {
            DB::rollBack();
            Log::error('Error creating the task -> ' . $e->getMessage());
            return response('Error creating the task -> ' . $e->getMessage(), 500);
        }
    }

    public function deleteTask($task_uid)
    {
        try {
            DB::beginTransaction();
            $task = $this->taskService->getTask($task_uid);
            $this->taskService->deleteTask($task);
            $categories_tasks = $this->categoryTaskService->getCategoriesTasksByTaskUid($task_uid);
            if (count($categories_tasks) !== 0) {
                $this->categoryTaskService->deleteCategoryTask($task_uid);
            }
            DB::commit();
            Log::info('Task deleted succesfully');
            $tasks = $this->taskService->getAllTasks();
            return response( $tasks , 200);
        }catch (ModelNotFoundException $e){
            DB::rollBack();
            Log::error('Fail deleting task');
            return response('Fail deleting task -> '.$e->getMessage(), 400);
        }catch(\Exception $e){
            DB::rollBack();
            Log::error('Fail deleting task');
            return response('Fail deleting task -> '.$e->getMessage(), 500);
        }
    }

    public function getAllTasks()
    {
        try{
            return $this->taskService->getAllTasks();
        }catch(\Exception $e){
            Log::error('Fail getting all tasks -> '.$e->getMessage());
            return response('Fail getting all tasks -> '.$e->getMessage(),500);
        }
    }
}
