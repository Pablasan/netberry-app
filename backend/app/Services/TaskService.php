<?php

declare(strict_types=1);

namespace App\Services;

use App\Task;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Str;

class TaskService
{
    public function createTask(string $task_name)
    {
        $task = new Task;
        $task->task_uid = Str::uuid();
        $task->task_name = $task_name;

        $task->save();

        return $task;
    }

   
    public function getTask(string $task_uid)
    {
        $task = Task::where('task_uid', $task_uid)->with('categories')->first();

        if(!$task) Throw new ModelNotFoundException('Task not found');

        return $task;
    }

    public function getAllTasks()
    {
        return Task::with('categories')->get();
    }

    public function deleteTask(object $task)
    {
        $task->delete();
    }
}