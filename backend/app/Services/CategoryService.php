<?php

declare(strict_types=1);

namespace App\Services;

use App\Category;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryService
{
    public function getCategory(string $category_uid)
    {
        $category = Category::where('category_uid', $category_uid)->first();

        if(!$category) Throw new ModelNotFoundException('Category not found');

        return $category;
    }
}
