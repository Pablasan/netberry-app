<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryTaskService
{
    public function createCategoryTask(string $category_uid, object $task_uid)
    {
        $category_task = DB::table('categories_tasks')->insert([
            'category_task_uid' => Str::uuid(),
            'category_uid' => $category_uid,
            'task_uid' => $task_uid,
        ]);

        return $category_task;
    }

    public function getCategoriesTasksByTaskUid(string $task_uid)
    {
        $categories_tasks = DB::table('categories_tasks')->where('task_uid', $task_uid)->get();

        return $categories_tasks;
    }

    public function deleteCategoryTask(string $task_uid)
    {
        DB::table('categories_tasks')->where('task_uid', $task_uid)->delete();
    }
}
