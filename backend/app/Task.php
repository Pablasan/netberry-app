<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $primaryKey = 'task_uid';
    public $incrementing = false;
    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';

    protected $fillable = [
        'task_uid',
        'task_name'
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_tasks', 'task_uid', 'category_uid')->withTimestamps();

    }
}
