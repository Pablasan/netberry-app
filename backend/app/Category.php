<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $primaryKey = 'category_uid';
    public $incrementing = false;
    // In Laravel 6.0+ make sure to also set $keyType
    protected $keyType = 'string';

    protected $fillable = [
        'category_uid',
        'category_name'
    ];

    public function tasks()
    {
        return $this->belongsToMany('App\Task', 'categories_tasks', 'task_uid', 'category_uid')->withTimestamps();

    }
}
