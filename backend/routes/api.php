<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('task', 'TaskController@createTask');
Route::delete('task/{task_uid}', 'TaskController@deleteTask');
Route::get('task', 'TaskController@getAllTasks');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


